﻿using System.Linq;
using System.Data;
using System.IO;
using System;
using System.Text;
using System.Data.SqlClient;

namespace Configurations
{
    class Program
    {
        // Connection to DataBase
        public static readonly string sqlconnectionstring = "Server=tcp:rmsdbdev.database.windows.net;Initial Catalog = Rms40CgiDemo; User ID = RmsAdmin; Password=Logica!123;";

        static void Main(string[] args)
        {
            //SqlCommand cmd = null;
            var dataTableId = new DataTable();
            dataTableId.Columns.Add("PPId", typeof(string));
            dataTableId.Columns.Add("AtId", typeof(int));
            
            // Reading csv file - turbines
            using (var srId = new StreamReader(@"tags_assets_id.csv"))
            {
                string line;
                string headerLine = srId.ReadLine();

                while ((line = srId.ReadLine()) != null)
                {
                    System.Diagnostics.Debug.WriteLine(line + "\n");

                    string[] lineItems = line.Split(',');

                    DataRow dr = dataTableId.NewRow();
                    dr["PPId"] = lineItems[0];
                    dr["AtId"] = lineItems[1];
                    dataTableId.Rows.Add(dr);
                }
            }

            // reading csv file - tags
            using (var srConfig = new StreamReader(@"tags_assets_config.csv"))
            {
                var tableConfig = new DataTable();
                tableConfig.Columns.Add("TagId", typeof(string));
                tableConfig.Columns.Add("assetType", typeof(string));
                tableConfig.Columns.Add("type", typeof(string));
                tableConfig.Columns.Add("class", typeof(int));
                tableConfig.Columns.Add("tag", typeof(string));
                tableConfig.Columns.Add("tableIndex", typeof(int));
                tableConfig.Columns.Add("assetGen", typeof(int));
                tableConfig.Columns.Add("rate", typeof(string));
                tableConfig.Columns.Add("unit", typeof(string));
                tableConfig.Columns.Add("monitoring", typeof(string));

                string headerLineConfig = srConfig.ReadLine();
                string lineConfig;
                while ((lineConfig = srConfig.ReadLine()) != null)
                {
                    System.Diagnostics.Debug.WriteLine(lineConfig + "\n");

                    string[] rowItems = lineConfig.Split(',');

                    DataRow dataRow = tableConfig.NewRow();
                    dataRow["TagId"] = rowItems[1];
                    dataRow["assettype"] = rowItems[2];
                    dataRow["type"] = rowItems[3];
                    dataRow["class"] = rowItems[4];
                    dataRow["tag"] = rowItems[5];
                    dataRow["tableIndex"] = rowItems[6];
                    dataRow["assetGen"] = rowItems[7];
                    dataRow["rate"] = rowItems[10];
                    dataRow["unit"] = rowItems[11];
                    dataRow["monitoring"] = rowItems[13];

                    tableConfig.Rows.Add(dataRow);
                }

                DataTable resulTable = new DataTable();
                resulTable = tableConfig.Clone();
                resulTable.Columns.Add("PowerPlantId", typeof(string));
                resulTable.Columns.Add("Assetype", typeof(int));
                resulTable.Columns.Add("dtHr", typeof(DateTime));
                resulTable.Columns.Add("userId", typeof(string));
                resulTable.Columns.Add("AssetSub", typeof(int));
                resulTable.Columns.Add("AssetMet", typeof(int));

                resulTable.Columns["TagId"].SetOrdinal(1);
                resulTable.Columns["PowerPlantId"].SetOrdinal(0);

                Random rdn = new Random();

                foreach (DataRow col in dataTableId.Rows)
                {
                    int numAleatorio = rdn.Next(1, 24);
                    var configQuery = tableConfig.AsEnumerable().Where(row => row.Field<int>("assetGen") == numAleatorio);

                    foreach (var item in configQuery)
                    {
                        DataRow line = resulTable.NewRow();

                        string user = "system";
                        DateTime dateTime = DateTime.Now;
                        string datetimestr = dateTime.ToString("yyyy-MM-dd HH:mm");
                        
                        line["PowerPlantId"] = col["PPId"];                     
                        line["TagId"] = item["TagId"];
                        line["assettype"] = item["assettype"];
                        line["type"] = item["type"];
                        line["class"] = item["class"];
                        line["tag"] = item["tag"];
                        line["tableIndex"] = item["tableIndex"];
                        line["assetGen"] = col["AtId"];
                        line["rate"] = item["rate"];
                        line["unit"] = item["unit"];
                        line["monitoring"] = !item["monitoring"].Equals("NULL") ? 1 : 0;
                        line["dtHr"] = dateTime;
                        line["userId"] = user;
                        
                        resulTable.Rows.Add(line);
                    }
                }

                // Export Data from "resultable" to "dbo.powerPlantTagIdV2"
                using (SqlConnection scnn = new SqlConnection(sqlconnectionstring))
                {
                    scnn.Open();

                    StringBuilder sb = new StringBuilder();

                    for (int i = 0; i < resulTable.Rows.Count; i++)
                    {
                        DataRow drResult = resulTable.Rows[i];

                        sb.Append("INSERT INTO dbo.powerPlantTagIdV2 ( powerPlantId, tagId, assetType, type, class, tag, tableIndex, assetGen, rate, unit, monitoring, dtHr, usrId) values('");
                        sb.Append(drResult.Field<string>("PowerPlantId").Trim() + "','");
                        sb.Append(drResult.Field<string>("TagId").ToString().Trim() + "','");
                        sb.Append(drResult.Field<string>("assettype").ToString().Trim() + "','");
                        sb.Append(drResult.Field<string>("type").ToString().Trim() + "','");
                        sb.Append(drResult.Field<int>("class").ToString().Trim() + "','");
                        sb.Append(drResult.Field<string>("tag").ToString().Trim() + "','");
                        sb.Append(drResult.Field<int>("tableIndex").ToString().Trim() + "','");
                        sb.Append(drResult.Field<int>("assetGen").ToString().Trim() + "','");
                        sb.Append(drResult.Field<string>("rate").ToString().Trim() + "','");
                        sb.Append(drResult.Field<string>("unit").ToString().Trim() + "','");
                        sb.Append(drResult.Field<string>("monitoring").ToString().Trim() + "','");
                        sb.Append(drResult.Field<DateTime>("dtHr").ToString().Trim() + "','");
                        sb.AppendLine(drResult.Field<string>("userId").ToString().Trim() + "')");
                    }

                    var cmd = new SqlCommand(sb.ToString(), scnn);
                    cmd.ExecuteNonQuery();
                }
            }
            Console.WriteLine("Load data successfully");
            Console.ReadLine();
        }
    }
}

//Escrever para ficheiro um CSV
//StringBuilder sbo = new StringBuilder();

//string[] columnNames = resulTable.Columns.Cast<DataColumn>().Select(column => column.ColumnName).ToArray();
//sbo.AppendLine(string.Join(";", columnNames));

//                foreach (DataRow row in resulTable.Rows)
//                {
//                    string[] fields = row.ItemArray.Select(field => field.ToString()).ToArray();
//sbo.AppendLine(string.Join(";", fields));
//                }

//                File.WriteAllText("configurations.csv", sbo.ToString());




